package cn.enncloud.iot.domain.repository;

import cn.enncloud.iot.domain.entity.MetricEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * <p>Title: MetricRepository.java</p>
 * <p>Description: 点表实体访问类 </p>
 * <p>Copyright: 新智云数据服务有限公司 2017 沪ICP备16024103号-1</p>
 *
 * @author lixiangk
 * @version 1.0 2018/7/31
 */
public interface MetricRepository extends JpaRepository<MetricEntity, Long>, JpaSpecificationExecutor<MetricEntity> {

    MetricEntity findFirstByOriginalMetric(String originalMetric);
}