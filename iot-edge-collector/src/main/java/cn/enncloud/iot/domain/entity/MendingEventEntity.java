package cn.enncloud.iot.domain.entity;

import cn.enncloud.iot.collector.event.MendingEventTypeEnum;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author lifeia
 * @date 2018/8/1.
 */
@Entity
@Table(name = "mending_event")
@Data
@ToString
public class MendingEventEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "start_time")
    private long startTime;

    @Column(name = "end_time")
    private long endTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "mending_event_type", length = 50)
    private MendingEventTypeEnum mendingEventTypeEnum;
    @Column
    private int cycle;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public MendingEventTypeEnum getMendingEventTypeEnum() {
        return mendingEventTypeEnum;
    }

    public void setMendingEventTypeEnum(MendingEventTypeEnum mendingEventTypeEnum) {
        this.mendingEventTypeEnum = mendingEventTypeEnum;
    }

    public int getCycle() {
        return cycle;
    }

    public void setCycle(int cycle) {
        this.cycle = cycle;
    }
}
