package cn.enncloud.iot.domain.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lifeia
 * @date 2018/8/2.
 */
@Component
@ConfigurationProperties(prefix = "enn.sqlite")
@Data
public class SqliteProperties {
    private String path;
}
