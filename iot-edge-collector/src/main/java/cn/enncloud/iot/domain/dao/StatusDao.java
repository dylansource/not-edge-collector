package cn.enncloud.iot.domain.dao;

/**
 * 运行状态，包含实时数据最新时间戳
 *
 * @author lifeia
 * @date 2018/7/31.
 */
public interface StatusDao {

    /**
     * 更新状态值
     *
     * @param statusKey   键
     * @param statusValue 状态值
     * @return
     */
    boolean update(String statusKey, String statusValue);

    /**
     * 获取运行状态值
     *
     * @param statusKey 键
     * @return 状态值
     */
    String get(String statusKey);
}
