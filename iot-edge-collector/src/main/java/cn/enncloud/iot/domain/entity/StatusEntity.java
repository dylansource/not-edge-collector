package cn.enncloud.iot.domain.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author lifeia
 * @date 2018/8/1.
 */
@Entity
@Table(name = "status")
@Data
@ToString
public class StatusEntity extends BaseEntity {

    @Id
    @Column(nullable = false, length = 128)
    private String key;
    @Column(nullable = false)
    private String value;
}
