package cn.enncloud.iot.domain.repository;

import cn.enncloud.iot.domain.entity.MendingEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lifeia
 * @date 2018/8/1.
 */
public interface MendingEventRepository extends JpaRepository<MendingEventEntity, Long> {

}
