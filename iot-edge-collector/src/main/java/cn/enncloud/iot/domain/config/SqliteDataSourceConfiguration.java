package cn.enncloud.iot.domain.config;

import ch.qos.logback.core.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;
import java.io.File;

/**
 * <p>Title: DataSourceConfiguration.java</p>
 * <p>Description: 数据源配置类 </p>
 * <p>Copyright: 新智云数据服务有限公司 2017 沪ICP备16024103号-1</p>
 *
 * @author lixiangk
 * @version 1.0 2018/7/31
 */
@Configuration
public class SqliteDataSourceConfiguration {

    @Autowired
    private SqliteProperties sqliteProperties;

    @Bean(destroyMethod = "", name = "EmbeddedDataSource")
    public DataSource dataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.sqlite.JDBC");
        mkDirIf(sqliteProperties.getPath());
        dataSourceBuilder.url("jdbc:sqlite:" + sqliteProperties.getPath());
        dataSourceBuilder.type(SQLiteDataSource.class);
        return dataSourceBuilder.build();
    }

    private void mkDirIf(String path) {
        File directory = new File(path);
        if (!directory.exists()) {
            FileUtil.createMissingParentDirectories(directory);
        }
    }
}
