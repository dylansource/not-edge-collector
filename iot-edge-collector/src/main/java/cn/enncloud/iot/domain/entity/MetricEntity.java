package cn.enncloud.iot.domain.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
@Entity
@Table(name = "metric")
@Data
@ToString
public class MetricEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    //原始点
    @Column(name = "original_metric", unique = true, nullable = false, length = 128)
    private String originalMetric;

    //cim标准点
    @Column(name = "cim_metric", unique = true, nullable = false, length = 128)
    private String cimMetric;

    @Column(name = "cim_standard_metric", unique = true, nullable = false, length = 128)
    private String cimStandardMetric;

    //cim标准点名称
    @Column(nullable = false)
    private String name;

    //站编号
    @Column(name = "station_id", nullable = false, length = 20)
    private String stationId;

    //站名称
    @Column(name = "station_name", length = 128)
    private String stationName;

    //项目编号
    @Column(name = "project_id", length = 10)
    private String projectId;

    //项目名称
    @Column(name = "project_name", length = 128)
    private String projectName;

    //cim设备类型
    @Column(name = "device_type", nullable = false, length = 20)
    private String deviceType;

    //单位
    @Column(length = 20)
    private String unit;

    //业务域
    @Column(length = 20)
    private String domain;

    @Column(name = "domain_name", length = 128)
    private String domainName;

    //设备编号
    @Column(name = "device_id", length = 128, nullable = false)
    private String deviceId;

    //cim量测属性
    @Column(name = "metric_attribute", length = 50, nullable = false)
    private String metricAttribute;

    //备注
    @Column
    private String remark;
}
