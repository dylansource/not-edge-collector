package cn.enncloud.iot.collector.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author lifeia
 * @date 2018/3/23.
 */
@Configuration
public class RetrofitConfig {

    //adapter地址
    @Value("${enn.adapter.baseUrl}")
    private String baseUrl;

    @Bean
    public Retrofit retrofit() throws Exception {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
