package cn.enncloud.iot.collector.controller;

import cn.enncloud.iot.collector.constant.CodeEnum;
import cn.enncloud.iot.collector.dto.response.DataRespBody;
import cn.enncloud.iot.collector.service.LoadCimConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "data")
public class loadMetricController {
    private static final Logger logger = LoggerFactory.getLogger(loadMetricController.class);

    @Autowired
    private LoadCimConfigService loadCimConfigService;

    @RequestMapping(value = "reloadCim")
    public DataRespBody loadCimConfig() {

        DataRespBody respBody = new DataRespBody();
        respBody.setCode(CodeEnum.IOT_SUCCESS.getCode());
        respBody.setMsg(CodeEnum.IOT_SUCCESS.getValue());
        respBody.setData(loadCimConfigService.loadMetric());
        return respBody;
    }
}
