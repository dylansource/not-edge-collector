package cn.enncloud.iot.collector.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static cn.enncloud.iot.collector.constant.Constant.REAL_TIME_SCHEDULED_CRON;

/**
 * @author
 * @date 2018/7/31.
 */
@Component
public class RealTimeDataCollectJob {
    private static final Logger logger = LoggerFactory.getLogger(RealTimeDataCollectJob.class);

    @Scheduled(cron = REAL_TIME_SCHEDULED_CRON)
    public void run() {
        // TODO: 2018/7/31  实时数据采集业务
        if (logger.isDebugEnabled()) {
            logger.debug("RealTimeDataCollectJob start");
        }
    }
}
