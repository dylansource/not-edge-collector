package cn.enncloud.iot.collector.service.impl;

import cn.enncloud.iot.collector.constant.RabbitMqConstant;
import cn.enncloud.iot.collector.dto.*;
import cn.enncloud.iot.collector.job.MendingDataCollectJob;
import cn.enncloud.iot.collector.service.MendingDataService;
import cn.enncloud.iot.commons.rabbit.produce.Producer;
import cn.enncloud.iot.domain.entity.MendingEventEntity;
import cn.enncloud.iot.domain.repository.MendingEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sunhongqiang
 * @date 2018/08/02
 */

@Service
public class MendingDataServiceImpl implements MendingDataService {
    private static final Logger logger = LoggerFactory.getLogger(MendingDataCollectJob.class);

    @Autowired
    private MendingEventRepository mendingEventRepository;

    @Autowired
    private Producer producer;

    /**
     * 获取第一个补招事件
     *
     * @return 补招事件数据
     */
    @Override
    public MendingEventEntity getFirstMendingEventEntity() {
        List<MendingEventEntity> mendingEventEntitieList = mendingEventRepository.findAll();
        if (mendingEventEntitieList != null && mendingEventEntitieList.size() > 0) {
            return mendingEventEntitieList.get(0);
        }
        return null;
    }

    /**
     * 删除补招事件
     *
     * @param id 事件id
     * @return
     */
    @Override
    public void deleteMendingEventEntityById(long id) {
        mendingEventRepository.delete(id);
    }

    /**
     * 发送历史数据到rabbitmq
     *
     * @param originalMetricDTOList
     */
    @Override
    public void sendHistoryDataToRabbitMQ(List<OriginalMetricDTO> originalMetricDTOList) {
        HeaderDTO headerDTO = new HeaderDTO();
        headerDTO.setV(RabbitMqConstant.RABBITMQ_DATA_HEADER);
        List<BodyDTO> bodyDTOList = new ArrayList<>();
        List<MetricDTO> metricDTOList = new ArrayList<>();
        BodyDTO bodyDTO = new BodyDTO();
        RabbitMqDataDTO rabbitMqDataDTO = new RabbitMqDataDTO();
        for (OriginalMetricDTO originalMetricDTO : originalMetricDTOList) {
            MetricDTO metricDTO = new MetricDTO();
            metricDTO.setN(originalMetricDTO.getOriginalMetric());
            metricDTO.setV(Float.valueOf(originalMetricDTO.getValue()));
            metricDTO.setT(originalMetricDTO.getTime());
            metricDTOList.add(metricDTO);
            if (metricDTOList.size() % RabbitMqConstant.RABBITMQ_METRIC_COUNT == 0) {
                bodyDTO.setM(metricDTOList);
                bodyDTOList.add(bodyDTO);
                rabbitMqDataDTO.setH(headerDTO);
                rabbitMqDataDTO.setB(bodyDTOList);
                try {
                    producer.send(rabbitMqDataDTO, RabbitMqConstant.RABBITMQ_ROUTEKEY);
                } catch (Exception e) {
                    logger.error("历史数据发送到rabbitmq失败！", e);
                }
                metricDTOList.clear();
            }
        }
        if (metricDTOList.size() > 0) {
            bodyDTO.setM(metricDTOList);
            bodyDTOList.add(bodyDTO);
            rabbitMqDataDTO.setH(headerDTO);
            rabbitMqDataDTO.setB(bodyDTOList);
            try {
                producer.send(rabbitMqDataDTO, RabbitMqConstant.RABBITMQ_ROUTEKEY);
            } catch (Exception e) {
                logger.error("历史数据发送到rabbitmq失败！", e);
            }
        }
    }
}
