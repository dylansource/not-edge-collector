package cn.enncloud.iot.collector.service;

import cn.enncloud.iot.collector.dto.OriginalMetricDTO;
import cn.enncloud.iot.domain.entity.MendingEventEntity;

import java.util.List;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
public interface MendingDataService {

    /**
     * 获取第一个补招事件
     *
     * @return 补招事件数据
     */
    MendingEventEntity getFirstMendingEventEntity();

    /**
     * 删除补招事件
     *
     * @param id 事件id
     * @return
     */
    void deleteMendingEventEntityById(long id);

    /**
     * 发送历史数据到rabbitmq
     *
     * @param originalMetricDTOList
     */
    void sendHistoryDataToRabbitMQ(List<OriginalMetricDTO> originalMetricDTOList);

}
