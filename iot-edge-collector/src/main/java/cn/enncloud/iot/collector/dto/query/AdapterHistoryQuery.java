package cn.enncloud.iot.collector.dto.query;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author sunhongqiang
 * @date 2018/08/08
 */
@Data
@ToString
public class AdapterHistoryQuery {
    private List<String> originalMetricList;
    private Long startTime;
    private Long endTime;
    private Integer interval;
}
