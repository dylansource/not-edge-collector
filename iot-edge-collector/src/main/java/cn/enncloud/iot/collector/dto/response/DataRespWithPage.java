package cn.enncloud.iot.collector.dto.response;

import java.util.List;

public class DataRespWithPage<T extends List> extends ResponseBody {

    private T data;

    private long totalCount;

    private int page;

    private int limit;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
