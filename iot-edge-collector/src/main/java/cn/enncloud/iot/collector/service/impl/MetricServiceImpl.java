package cn.enncloud.iot.collector.service.impl;

import cn.enncloud.iot.collector.constant.StaticManager;
import cn.enncloud.iot.collector.dto.StationDTO;
import cn.enncloud.iot.collector.service.LoadCimConfigService;
import cn.enncloud.iot.collector.service.MetricService;
import cn.enncloud.iot.domain.entity.MetricEntity;
import cn.enncloud.iot.domain.repository.MetricRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.*;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
@Service
public class MetricServiceImpl implements MetricService {
    private static final Logger logger = LoggerFactory.getLogger(MetricServiceImpl.class);

    @Autowired
    private MetricRepository metricRepository;

    @Autowired
    private LoadCimConfigService loadCimConfigService;

    /**
     * 点表数据导入
     *
     * @param inputStream 点表流数据
     * @return 数据错误日志
     */
    @Override
    public String upload(InputStream inputStream, boolean isCheck) {

        List<MetricEntity> metricEntityList = new ArrayList<>();
        StringBuilder errorLog = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream, Charset.forName("GB2312")))) {
            String lineStr;
            int columnIndex = 0;

            StringBuilder columnErrorInfo = new StringBuilder();
            while ((lineStr = bufferedReader.readLine()) != null) {
                columnIndex++;
                if (columnIndex == 1) {
                    continue;
                }
                String[] columnArray = lineStr.split(",");

                int columnLen = columnArray.length;
                if (columnLen >= 5) {
                    //原始点
                    String originalMetric = columnArray[0].trim();
                    String originalMetricErrorInfo = checkOriginalMetric(originalMetric);
                    if (StringUtils.isNoneEmpty(originalMetricErrorInfo)) {
                        columnErrorInfo.append(originalMetricErrorInfo).append(";");
                    }
                    //cim标准点
                    String cimMetric = columnArray[1].trim();
                    String cimMetricErrorInfo = checkCimMetric(cimMetric);
                    if (StringUtils.isNoneEmpty(cimMetricErrorInfo)) {
                        columnErrorInfo.append(cimMetricErrorInfo).append(";");
                    }
                    //cim标准点名称
                    String name = columnArray[2].trim();
                    String nameErrorInfo = checkName(name);
                    if (StringUtils.isNoneEmpty(nameErrorInfo)) {
                        columnErrorInfo.append(nameErrorInfo).append(";");
                    }
                    //站id
                    String stationId = columnArray[4].trim();
                    String stationIdErrorInfo = checkStationId(stationId);
                    if (StringUtils.isNoneEmpty(stationIdErrorInfo)) {
                        columnErrorInfo.append(stationIdErrorInfo);
                    }

                    if (columnErrorInfo.length() > 0) {
                        columnErrorInfo.insert(0, "第" + columnIndex + "行:").append("\n").toString();
                        errorLog.append(columnErrorInfo);
                        columnErrorInfo.delete(0, columnErrorInfo.length());
                    } else {
                        // TODO: 2018/7/31  保存数据
                        String unit = columnArray[3];
                        String stationName = columnLen > 5 ? columnArray[5] : "";
                        String projectId = columnLen > 6 ? columnArray[6] : "";
                        String projectName = columnLen > 7 ? columnArray[7] : "";
                        String domain = columnLen > 8 ? columnArray[8] : "";
                        String domainName = columnLen > 9 ? columnArray[9] : "";
                        String remark = columnLen > 10 ? columnArray[10] : "";
                        MetricEntity metricEntity = new MetricEntity();
                        metricEntity.setOriginalMetric(originalMetric);
                        metricEntity.setCimMetric(cimMetric);
                        metricEntity.setName(name);
                        metricEntity.setStationId(stationId);
                        metricEntity.setStationName(stationName);
                        metricEntity.setUnit(unit);
                        metricEntity.setProjectId(projectId);
                        metricEntity.setProjectName(projectName);
                        metricEntity.setDomain(domain);
                        metricEntity.setDomainName(domainName);
                        metricEntity.setRemark(remark);
                        String[] cimArray = cimMetric.split("\\\\");

                        if (cimArray.length == 4) {
                            metricEntity.setDeviceType(cimArray[1]);
                            metricEntity.setDeviceId(cimArray[2]);
                            metricEntity.setMetricAttribute(cimArray[3]);
                            metricEntity.setCimStandardMetric(MessageFormat.format("{0}_{1}_{2}", cimArray[1], cimArray[2], cimArray[3]));
                        } else {
                            metricEntity.setDeviceType(cimArray[0]);
                            metricEntity.setDeviceId(cimArray[1]);
                            metricEntity.setMetricAttribute(cimArray[2]);
                            metricEntity.setCimStandardMetric(MessageFormat.format("{0}_{1}_{2}", cimArray[0], cimArray[1], cimArray[2]));
                        }
                        metricEntityList.add(metricEntity);
                    }

                } else {
                    errorLog.append("第" + columnIndex + "行:列数据缺少\n");
                }
            }

        } catch (Exception ex) {
            logger.error("文件上传报错", ex);
            errorLog.append(ex.getMessage());
        }
        if (errorLog.length() > 0) {
            return errorLog.toString();
        }
        if (!isCheck) {
            try {
                metricRepository.deleteAllInBatch();
                metricRepository.save(metricEntityList);
                //更新缓存中的点表数据
                loadCimConfigService.loadMetric();
            } catch (Exception ex) {
                logger.error("点表数据保存错误", ex);
                return ex.getMessage();
            }
        }
        return null;
    }

    private String checkStationId(String value) {
        //  站id校验 ,返回错误信息
        if (StringUtils.isEmpty(value)) {
            return "站Id不能为空";
        }
        if (value.length() < 8) {
            return "站Id字段长度不能少于8位";
        }
        return null;
    }

    private String checkName(String value) {
        // 测量点名称校验 ,返回错误信息
        if (StringUtils.isEmpty(value)) {
            return "CIM标准点名称不能为空";
        }
        return null;
    }

    private String checkCimMetric(String value) {
        // TODO: 2018/7/31 cim点校验逻辑,返回错误信息
        return null;
    }

    private String checkOriginalMetric(String value) {
        // TODO: 2018/7/31 原测量点校验逻辑,返回错误信息

        return null;
    }

    @Override
    public String download() {

        StringBuilder metricData = new StringBuilder();
        List<MetricEntity> metricEntityList = metricRepository.findAll();
        metricData.append("old,new,desc,unit,站编号,站名称,项目编号,项目名称,业务域编号,业务域名称,备注\n");
        for (MetricEntity metricEntity : metricEntityList) {
            metricData.append(metricEntity.getOriginalMetric()).append(",")
                    .append(metricEntity.getCimMetric()).append(",")
                    .append(metricEntity.getName()).append(",")
                    .append(metricEntity.getUnit()).append(",")
                    .append(metricEntity.getStationId()).append(",")
                    .append(metricEntity.getStationName()).append(",")
                    .append(metricEntity.getProjectId()).append(",")
                    .append(metricEntity.getProjectName()).append(",")
                    .append(metricEntity.getDomain()).append(",")
                    .append(metricEntity.getDomainName()).append(",")
                    .append(metricEntity.getRemark()).append("\n");
        }
        return metricData.toString();
    }

    /**
     * 获取原始点信息
     *
     * @param stationId      站id
     * @param deviceType     CIM设备类型
     * @param originalMetric 采集点名称
     */
    @Override
    public List<String> findOriginalMetricList(String stationId, String deviceType, String originalMetric) {

        List<String> originalMetricList = new ArrayList<>();
        List<MetricEntity> metricEntityList = metricRepository.findAll(buildSpecification(stationId, deviceType, originalMetric));
        for (MetricEntity m : metricEntityList) {
            originalMetricList.add(m.getOriginalMetric());
        }
        return originalMetricList;
    }

    private Specification<MetricEntity> buildSpecification(String stationId, String deviceType, String originalMetric) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (StringUtils.isNotBlank(stationId)) {
                predicates.add(criteriaBuilder.equal(root.get("stationId"), stationId));
            }
            if (StringUtils.isNotBlank(stationId)) {
                predicates.add(criteriaBuilder.equal(root.get("deviceType"), deviceType));
            }
            if (StringUtils.isNotBlank(stationId)) {
                predicates.add(criteriaBuilder.equal(root.get("originalMetric"), originalMetric));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        };
    }

    /**
     * 查询站信息列表
     *
     * @return 站信息列表
     */
    @Override
    public List<StationDTO> getStationList() {
        List<MetricEntity> metricEntityList = StaticManager.IOT_METRIC_LIST;
        if (null != metricEntityList && metricEntityList.size() > 0) {
            Map<String, StationDTO> stationMap = new HashMap<>();
            for (MetricEntity metricEntity : metricEntityList) {
                if (null == stationMap.get(metricEntity.getStationId())) {
                    StationDTO stationDTO = new StationDTO();
                    stationDTO.setStationId(metricEntity.getStationId());
                    stationDTO.setStationName(metricEntity.getStationName());
                    stationMap.put(metricEntity.getStationId(), stationDTO);
                }
            }
            return new ArrayList<>(stationMap.values());
        }
        return new ArrayList<>();
    }

    /**
     * 通过stationId查询设备类型列表
     *
     * @param stationId 站Id
     * @return 设备类型列表
     */
    @Override
    public List<String> getDeviceTypeList(String stationId) {
        List<MetricEntity> metricEntityList = StaticManager.IOT_METRIC_LIST;
        if (null != metricEntityList && metricEntityList.size() > 0) {
            HashSet<String> deviceTypeSet = new HashSet<>();
            if (StringUtils.isEmpty(stationId)) {
                for (MetricEntity metricEntity : metricEntityList) {
                    if (!deviceTypeSet.contains(metricEntity.getDeviceType())) {
                        deviceTypeSet.add(metricEntity.getDeviceType());
                    }
                }
            } else {
                for (MetricEntity metricEntity : metricEntityList) {
                    if (StringUtils.equals(metricEntity.getStationId(), stationId) && !deviceTypeSet.contains(metricEntity.getDeviceType())) {
                        deviceTypeSet.add(metricEntity.getDeviceType());
                    }
                }
            }
            return new ArrayList<>(deviceTypeSet);
        }
        return new ArrayList<>();
    }
}
