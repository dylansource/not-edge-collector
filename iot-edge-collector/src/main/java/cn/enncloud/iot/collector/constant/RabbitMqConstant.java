package cn.enncloud.iot.collector.constant;

/**
 * @author sunhongqiang
 * @date 2018/08/08
 */
public interface RabbitMqConstant {

    /**
     * 每个数据包最多发送测量点数
     */
    int RABBITMQ_METRIC_COUNT = 300;

    /**
     * rabbitmq数据格式版本
     */
    String RABBITMQ_DATA_HEADER = "1.1";

    /**
     * routeKey
     */
    String RABBITMQ_ROUTEKEY = "resume.uncim";
}
