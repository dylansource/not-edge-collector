package cn.enncloud.iot.collector.controller;

import cn.enncloud.iot.collector.constant.CodeEnum;
import cn.enncloud.iot.collector.dto.OriginalMetricDTO;
import cn.enncloud.iot.collector.dto.response.DataRespBody;
import cn.enncloud.iot.collector.service.AdapterService;
import cn.enncloud.iot.collector.service.MendingDataService;
import cn.enncloud.iot.collector.service.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sunhongqiang
 * @date 2018/08/08
 */
@RestController
public class HistoryDataController {

    @Autowired
    private AdapterService adapterService;

    @Autowired
    private MetricService metricService;

    @Autowired
    private MendingDataService mendingDataService;

    /**
     * 手动补招
     *
     * @param deviceType
     * @param beginTime
     * @param endTime
     * @param stationId
     * @param originalMetric
     * @return
     */
    @RequestMapping(value = "api/v1/collector/data/inviting")
    public DataRespBody dataInvitingMethod(@RequestParam(value = "beginTime") Long beginTime,
                                           @RequestParam(value = "endTime") Long endTime,
                                           @RequestParam(value = "stationId", required = false) String stationId,
                                           @RequestParam(value = "deviceType", required = false) String deviceType,
                                           @RequestParam(value = "originalMetric", required = false) String originalMetric) {
        DataRespBody respBody = new DataRespBody();
        respBody.setCode(CodeEnum.IOT_SUCCESS.getCode());
        respBody.setMsg(CodeEnum.IOT_SUCCESS.getValue());
        List<String> originalMetricList = metricService.findOriginalMetricList(stationId, deviceType, originalMetric);
        List<OriginalMetricDTO> originalMetricDTOList;
        if (originalMetricList != null && originalMetricList.size() > 0) {
            originalMetricDTOList = adapterService.getHistoryData(beginTime, endTime, originalMetricList, null);
            if (originalMetricDTOList != null && originalMetricDTOList.size() > 0) {
                mendingDataService.sendHistoryDataToRabbitMQ(originalMetricDTOList);
            }
        }
        return respBody;
    }

    /**
     * 历史数据查询
     *
     * @param beginTime
     * @param endTime
     * @param originalMetric
     * @return
     */
    @RequestMapping(value = "api/v1/collector/data/history")
    public DataRespBody historyDataMethod(@RequestParam(value = "beginTime") Long beginTime,
                                          @RequestParam(value = "endTime") Long endTime,
                                          @RequestParam(value = "originalMetric") String originalMetric) {
        DataRespBody respBody = new DataRespBody();
        respBody.setCode(CodeEnum.IOT_SUCCESS.getCode());
        respBody.setMsg(CodeEnum.IOT_SUCCESS.getValue());
        List<String> originalMetricList = new ArrayList<>();
        originalMetricList.add(originalMetric);
        List<OriginalMetricDTO> originalMetricDTOList = adapterService.getHistoryData(beginTime, endTime, originalMetricList, null);
        respBody.setData(originalMetricDTOList);
        return respBody;
    }
}
