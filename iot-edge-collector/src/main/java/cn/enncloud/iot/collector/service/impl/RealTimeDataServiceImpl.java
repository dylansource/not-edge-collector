package cn.enncloud.iot.collector.service.impl;

import cn.enncloud.iot.collector.constant.AdapterInfo;
import cn.enncloud.iot.collector.constant.Constant;
import cn.enncloud.iot.collector.constant.StaticManager;
import cn.enncloud.iot.collector.dto.AdapterRealMetricDTO;
import cn.enncloud.iot.collector.dto.OriginalMetricDTO;
import cn.enncloud.iot.collector.dto.StationInfoDTO;
import cn.enncloud.iot.collector.dto.rabbitmq.RabbitMqDTO;
import cn.enncloud.iot.collector.dto.response.DataRespBody;
import cn.enncloud.iot.collector.service.RealTimeDataService;
import cn.enncloud.iot.domain.entity.MetricEntity;
import cn.enncloud.iot.domain.entity.StatusEntity;
import cn.enncloud.iot.domain.repository.MetricRepository;
import cn.enncloud.iot.domain.repository.StatusRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@EnableBinding(Source.class)
@Service
public class RealTimeDataServiceImpl implements RealTimeDataService {

    private static final Logger logger = LogManager.getLogger(RealTimeDataServiceImpl.class);

    @Autowired
    private MetricRepository metricRepository;

    @Autowired
    private AdapterInfo adapterInfo;

    @Resource
    private MessageChannel output;

    @Autowired
    private StatusRepository statusRepository;

    //    @Autowired
//    private  OkHttpClient okHttpClient;
    @Override
    public AdapterRealMetricDTO getRealTimeData(List<String> originalMetricDTO) {

        OkHttpClient okHttpClient = new OkHttpClient();

        ObjectMapper mapper = new ObjectMapper();
        String queryString = "";
        try {
            queryString = mapper.writeValueAsString(originalMetricDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(adapterInfo.getUrl()).append(queryString);
        String res = "";
        Request request = new Request.Builder()
                .url(adapterInfo.getUrl())
                .build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            res = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (logger.isInfoEnabled()) {
            logger.info(MessageFormat.format("adapter返回参数[{0}]", res));
        }


        DataRespBody<AdapterRealMetricDTO> respBody = null;
        try {
            respBody = mapper.readValue(res, DataRespBody.class);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return respBody.getData();
    }

    public AdapterRealMetricDTO realtimeDataCollector(List<OriginalMetricDTO> originalMetricDTO) {
        List<String> list = StaticManager.IOT_METRIC_LIST.stream().map(s -> s.getOriginalMetric()).collect(Collectors.toList());

        OkHttpClient okHttpClient = new OkHttpClient();

        ObjectMapper mapper = new ObjectMapper();
        String queryString = "";
        try {
            queryString = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(adapterInfo.getUrl()).append(queryString);
        String res = "";
        Request request = new Request.Builder()
                .url(adapterInfo.getUrl())
                .build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            res = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (logger.isInfoEnabled()) {
            logger.info(MessageFormat.format("adapter返回参数[{0}]", res));
        }


        AdapterRealMetricDTO adapterRealMetricDTO = null;
        try {
            adapterRealMetricDTO = mapper.readValue(res, AdapterRealMetricDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return adapterRealMetricDTO;
    }

    @Override
    public long getRealTimeStamp(String statusKey) {
        StatusEntity statusEntity = statusRepository.findOne(Constant.REAL_TIME_DEFAULT);
        return new Long(null == statusEntity ? (System.currentTimeMillis() / 1000) + "" : statusEntity.getValue());
    }

    @Override
    public StationInfoDTO findStationInfo() throws MalformedURLException {
        URL url = new URL(adapterInfo.getUrl());
        StationInfoDTO stationInfoDTO = new StationInfoDTO();
        stationInfoDTO.setIp(url.getHost());
        stationInfoDTO.setPort(String.valueOf(url.getPort()));
        stationInfoDTO.setMaster(adapterInfo.getMaster());

        return stationInfoDTO;
    }


    @Override
    public boolean sendToRabbitmq(RabbitMqDTO rabbitMqDTO) {


        return this.output.send(MessageBuilder.withPayload(rabbitMqDTO).build()); // 创建并发送消息
    }

    @Override
    public void updateRealTimeStamp(String statusKey, long timestamp) {

        StatusEntity statusEntity = new StatusEntity();
        statusEntity.setKey(statusKey);
        statusEntity.setValue(String.valueOf(timestamp));

        statusRepository.saveAndFlush(statusEntity);

    }

    @Override
    public List<MetricEntity> findAll(MetricEntity entity) {
        return this.metricRepository.findAll((root, query, cb) -> {
            List<javax.persistence.criteria.Predicate> predicatesList = new ArrayList<>();

            if (!StringUtils.isEmpty(entity.getId()) && entity.getId() != 0) {
                Predicate id = cb.equal(root.get("id").as(Long.class), entity.getId());
                predicatesList.add(id);
            }
            if (!StringUtils.isEmpty(entity.getName())) {
                Predicate name = cb.like(root.get("name").as(String.class), "%" + entity.getName() + "%");
                predicatesList.add(name);

            }
            if (!StringUtils.isEmpty(entity.getDeviceType())) {
                Predicate deviceType = cb.equal(root.get("deviceType").as(String.class), entity.getDeviceType());
                predicatesList.add(deviceType);

            }
            if (!StringUtils.isEmpty(entity.getCimMetric())) {
                Predicate cimMetric = cb.like(root.get("cimMetric").as(String.class), "%" + entity.getCimMetric() + "%");
                predicatesList.add(cimMetric);

            }
            if (!StringUtils.isEmpty(entity.getOriginalMetric())) {
                Predicate originalMetric = cb.like(root.get("originalMetric").as(String.class), "%" + entity.getOriginalMetric() + "%");
                predicatesList.add(originalMetric);

            }


            Predicate[] predicates = new javax.persistence.criteria.Predicate[predicatesList.size()];
            return cb.and(predicatesList.toArray(predicates));
        });
    }


    public static void main(String[] args) throws IOException {
        RealTimeDataServiceImpl example = new RealTimeDataServiceImpl();
//        String response = example.run("https://raw.github.com/square/okhttp/master/README.md");
    }
}
