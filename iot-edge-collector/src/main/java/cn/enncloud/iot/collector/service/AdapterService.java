package cn.enncloud.iot.collector.service;

import cn.enncloud.iot.collector.dto.OriginalMetricDTO;

import java.util.List;

/**
 * @author sunhongqiang
 * @date 2018/08/08
 * adapter接口
 */
public interface AdapterService {

    /**
     * 查询适配器获取历史数据
     *
     * @param startTime          开始时间
     * @param endTime            结束时间
     * @param originalMetricList 点表list
     * @param interval              采集周期
     * @return 点表历史数据
     */
    List<OriginalMetricDTO> getHistoryData(Long startTime, Long endTime, List<String> originalMetricList, Integer interval);

    /**
     * 从站原始点查询
     */
    List<String> getMetricFromAdapter();

}
