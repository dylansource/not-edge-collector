package cn.enncloud.iot.collector.dto.rabbitmq;

import java.util.List;

public class BodyDTO {
    private Long t;

    private List<MetricsDTO> m;

    public Long getT() {
        return t;
    }

    public void setT(Long t) {
        this.t = t;
    }

    public List<MetricsDTO> getM() {
        return m;
    }

    public void setM(List<MetricsDTO> m) {
        this.m = m;
    }

    @Override
    public String toString() {
        return "BodyDTO{" +
                "t=" + t +
                ", m=" + m +
                '}';
    }
}
