package cn.enncloud.iot.collector.dto.rabbitmq;

public class MetricsDTO {
    private String n;
    private Float v;
    private Long t;

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public Float getV() {
        return v;
    }

    public void setV(Float v) {
        this.v = v;
    }

    public Long getT() {
        return t;
    }

    public void setT(Long t) {
        this.t = t;
    }

    @Override
    public String toString() {
        return "MetricsDTO{" +
                "n='" + n + '\'' +
                ", v=" + v +
                ", t=" + t +
                '}';
    }
}
