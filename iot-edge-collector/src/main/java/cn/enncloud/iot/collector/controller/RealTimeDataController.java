package cn.enncloud.iot.collector.controller;

import cn.enncloud.iot.collector.constant.CodeEnum;
import cn.enncloud.iot.collector.dto.AdapterRealMetricDTO;
import cn.enncloud.iot.collector.dto.RealTimeDataDTO;
import cn.enncloud.iot.collector.dto.response.DataRespBody;
import cn.enncloud.iot.collector.service.RealTimeDataService;
import cn.enncloud.iot.domain.entity.MetricEntity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "data")
public class RealTimeDataController {

    private static final Logger logger = LogManager.getLogger(RealTimeDataController.class);

    @Autowired
    private RealTimeDataService realTimeDataService;
    @RequestMapping(value = "/realtimeMethod")
    public DataRespBody realtimeMethod(@RequestParam(value = "deviceType", required = false) String deviceType,
                                       @RequestParam(value = "metricId", required = false) Integer metricId,
                                       @RequestParam(value = "metricName", required = false) String metricName,
                                       @RequestParam(value = "cimMetric", required = false) String cimMetric,
                                       @RequestParam(value = "originalMetric", required = false) String originalMetric) {


        DataRespBody respBody = new DataRespBody();
        respBody.setCode(CodeEnum.IOT_SUCCESS.getCode());
        respBody.setMsg(CodeEnum.IOT_SUCCESS.getValue());
        List<RealTimeDataDTO> list = new ArrayList<>();
        MetricEntity entity = new MetricEntity();
        if (metricId != null) {
            entity.setId(metricId);
        }
        entity.setDeviceType(deviceType);
        entity.setName(metricName);
        entity.setCimMetric(cimMetric);
        entity.setOriginalMetric(originalMetric);
        List<MetricEntity> entityList = realTimeDataService.findAll(entity);

        List<String> originalMetricDTO = new ArrayList<>();
        if (entityList != null && entityList.size() > 0) {
            originalMetricDTO = entityList.stream().map(s -> s.getOriginalMetric()).collect(Collectors.toList());
        }

//        originalMetricDTO.add("S1\\B\\AV1_feedback");
//        originalMetricDTO.add("S1\\B\\T1");
        AdapterRealMetricDTO adapterRealMetricDTO = realTimeDataService.getRealTimeData(originalMetricDTO);
        Long time = adapterRealMetricDTO.getTime();
        adapterRealMetricDTO.getMetricValueList().forEach(s -> {
            RealTimeDataDTO realTimeDataDTO = new RealTimeDataDTO();
            realTimeDataDTO.setOriginalMetric(s.getOriginalMetric());
            realTimeDataDTO.setValue(s.getValue());
            realTimeDataDTO.setTime(time);
            for (MetricEntity metricEntity : entityList) {

                if (metricEntity.getOriginalMetric().equalsIgnoreCase(s.getOriginalMetric())) {
                    realTimeDataDTO.setCimMetric(metricEntity.getCimMetric());
                    realTimeDataDTO.setMetricName(metricEntity.getName());
                    break;
                }
            }

            list.add(realTimeDataDTO);

        });

        respBody.setData(list);

        return respBody;

    }
    @RequestMapping(value = "/infoMethod")
    public DataRespBody infoMethod(){
        DataRespBody respBody = new DataRespBody();
        respBody.setCode(CodeEnum.IOT_SUCCESS.getCode());
        respBody.setMsg(CodeEnum.IOT_SUCCESS.getValue());
        try {
            respBody.setData(realTimeDataService.findStationInfo());
        } catch (MalformedURLException e) {
            respBody.setCode(CodeEnum.IOT_EXCEPTION.getCode());
            respBody.setMsg(CodeEnum.IOT_EXCEPTION.getValue());
            e.printStackTrace();
            if(logger.isErrorEnabled()){
                logger.error(MessageFormat.format("从站信息查询接口获取配置文件错误原因【{0}】",e.getMessage()));
            }
        }
        return respBody;
    }
}
