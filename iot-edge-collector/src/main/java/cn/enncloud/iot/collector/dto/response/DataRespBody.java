package cn.enncloud.iot.collector.dto.response;

import java.util.List;

public class DataRespBody<T extends Object> extends ResponseBody {

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
