package cn.enncloud.iot.collector.service;

import cn.enncloud.iot.collector.dto.StationDTO;

import java.io.InputStream;
import java.util.List;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
public interface MetricService {

    /**
     * 点表数据导入
     *
     * @param inputStream 点表流数据
     * @return 数据错误日志
     */
    String upload(InputStream inputStream, boolean isCheck);

    /**
     * 点表数据导出
     *
     * @return 点表数据
     */
    String download();

    /**
     * 查询站信息列表
     *
     * @return 站信息列表
     */
    List<StationDTO> getStationList();

    /**
     * 通过stationId查询设备类型列表
     *
     * @param stationId 站Id
     * @return 设备类型列表
     */
    List<String> getDeviceTypeList(String stationId);

    /**
     * 获取原始点信息
     *
     * @param stationId      站id
     * @param deviceType     CIM设备类型
     * @param originalMetric 采集点名称
     */
    List<String> findOriginalMetricList(String stationId, String deviceType, String originalMetric);
}
