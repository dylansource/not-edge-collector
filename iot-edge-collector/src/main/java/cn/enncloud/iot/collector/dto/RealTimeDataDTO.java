package cn.enncloud.iot.collector.dto;

public class RealTimeDataDTO {

    private String metricName;

    private String cimMetric;

    private String originalMetric;

    private Float value;

    private Long time;

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public String getCimMetric() {
        return cimMetric;
    }

    public void setCimMetric(String cimMetric) {
        this.cimMetric = cimMetric;
    }

    public String getOriginalMetric() {
        return originalMetric;
    }

    public void setOriginalMetric(String originalMetric) {
        this.originalMetric = originalMetric;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
