package cn.enncloud.iot.collector.event;

import cn.enncloud.iot.collector.service.RealTimeDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import static cn.enncloud.iot.collector.constant.Constant.REAL_TIME_DEFAULT;

/**
 * @author lifeia
 * @date 2018/8/3.
 */

@Order(1)
@Component
public class StartUpApplicationRunner implements ApplicationRunner, ApplicationEventPublisherAware {
    private static final Logger logger = LoggerFactory.getLogger(StartUpApplicationRunner.class);

    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired(required = false) // TODO: 2018/8/3 delete
    private RealTimeDataService realTimeDataService;
    private static int THOUSAND = 1000;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {

        long startTime = realTimeDataService.getRealTimeStamp(REAL_TIME_DEFAULT);
        Long endTime = System.currentTimeMillis();
        // TODO: 2018/8/3 取配置
        int cycle = 60;
        //当前时间与实时数据时间戳的时间差小于采集周期的1.5倍
        if ((endTime - startTime) / THOUSAND < cycle * 1.5) {
            return;
        }

        this.applicationEventPublisher.publishEvent(new MendingEvent("重启补招", startTime, endTime, MendingEventTypeEnum.ApplicationStartUp, cycle));
        if (logger.isDebugEnabled()) {
            logger.debug("触发重启,start:{},end:{}", startTime, endTime);
        }
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
