package cn.enncloud.iot.collector.service;

import cn.enncloud.iot.collector.dto.OriginalMetricDTO;
import cn.enncloud.iot.collector.dto.query.AdapterHistoryQuery;
import cn.enncloud.iot.collector.dto.response.DataRespBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

import java.util.List;

/**
 * @author sunhongqiang
 * @date 2018/08/02.
 */
public interface AdapterRetrofitService {

    /**
     * 实时数据查询
     *
     * @param originalMetricList 从站原始点名称
     */
    @POST("/api/v1/adapter/data/realtime")
    @Headers({"Content-Type:application/json;charset=UTF-8"})
    Call<DataRespBody> getRealtimeDataFromAdapter(@Body List<String> originalMetricList);

    /**
     * 历史数据查询
     *
     * @param adapterHistoryQuery 从站原始点名称
     */
    @POST("/api/v1/adapter/data/history")
    @Headers({"Content-Type:application/json;charset=UTF-8"})
    Call<DataRespBody<List<OriginalMetricDTO>>> getHistoryDataFromAdapter(@Body AdapterHistoryQuery adapterHistoryQuery);

    /**
     * 从站原始点查询
     */
    @GET("/api/v1/adapter/metric")
    @Headers({"Content-Type:application/json;charset=UTF-8"})
    Call<DataRespBody<List<String>>> getMetricFromAdapter();

    /**
     * 从站系统连通性验证
     *
     * @param ip
     * @param port
     */
    @POST("/api/v1/adapter/connect/check")
    @Headers({"Content-Type:application/json;charset=UTF-8"})
    Call<DataRespBody> getConnectCheckFromAdapter(@Body String ip, @Body String port);

}
