package cn.enncloud.iot.collector.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class StationInfoDTO {

    private Integer master;

    private  String ip;

    private String port;

}
