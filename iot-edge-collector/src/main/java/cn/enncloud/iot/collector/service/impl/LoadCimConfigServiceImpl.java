package cn.enncloud.iot.collector.service.impl;

import cn.enncloud.iot.collector.constant.StaticManager;
import cn.enncloud.iot.collector.service.LoadCimConfigService;
import cn.enncloud.iot.domain.entity.MetricEntity;
import cn.enncloud.iot.domain.repository.MetricRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoadCimConfigServiceImpl implements LoadCimConfigService {
    private static final Logger logger = LogManager.getLogger(LoadCimConfigServiceImpl.class);

    @Autowired
    private MetricRepository metricRepository;


    @Override
    public List<MetricEntity> loadMetric() {
        List<MetricEntity> metricEntityList = metricRepository.findAll();

        metricEntityList.forEach(s -> {
            if (logger.isInfoEnabled()) logger.info(s.toString());
        });
        StaticManager.IOT_METRIC_LIST = metricEntityList;
        return StaticManager.IOT_METRIC_LIST;
    }

    @Override
    public List<MetricEntity> findMetricList() {

        return metricRepository.findAll();
    }
}
