package cn.enncloud.iot.collector.constant;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
public class Constant {

    /**
     * 实时数据采集任务执行频率
     */
    public static final String REAL_TIME_SCHEDULED_CRON = "${job.real-time.cron}";

    /**
     * 补招数据采集任务执行频率
     */
    public static final String MENDING_SCHEDULED_FIXED_DELAY = "${job.mending.fixedDelay}";

    /**
     * 最新时间戳键值
     */
    public static final String REAL_TIME_DEFAULT = "real_time_default";

}
