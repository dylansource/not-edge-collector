package cn.enncloud.iot.collector.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
@Data
@ToString
public class MetricDTO {

    /**
     * 采集点originName
     */
    private String n;
    /**
     * 值
     */
    private Float v;
    /**
     * 时间戳
     */
    private Long t;


}
