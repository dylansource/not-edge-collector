package cn.enncloud.iot.collector.dto;

import cn.enncloud.iot.commons.util.JsonUtils;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sunhongqiang
 * @date 2018/08/06
 * rabbitMq数据格式body
 */
@Data
@ToString
public class BodyDTO {

    /**
     * time
     */
    private Long t;

    /**
     * metrics
     */
    private List<MetricDTO> m;

    public static void main(String[] args) {
        List<String> originalMetricList = new ArrayList<>();
        originalMetricList.add("S1\\B\\AV1_feedback");
        originalMetricList.add("S1\\B\\T1");
        originalMetricList.add("S2\\T\\TI_203");
        System.out.println(JsonUtils.writeValueAsString(originalMetricList));
    }

}
