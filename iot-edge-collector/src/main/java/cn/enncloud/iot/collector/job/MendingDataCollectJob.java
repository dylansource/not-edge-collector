package cn.enncloud.iot.collector.job;

import cn.enncloud.iot.collector.dto.OriginalMetricDTO;
import cn.enncloud.iot.collector.service.AdapterService;
import cn.enncloud.iot.collector.service.LoadCimConfigService;
import cn.enncloud.iot.collector.service.MendingDataService;
import cn.enncloud.iot.domain.entity.MendingEventEntity;
import cn.enncloud.iot.domain.entity.MetricEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static cn.enncloud.iot.collector.constant.Constant.MENDING_SCHEDULED_FIXED_DELAY;

/**
 * @author
 * @date 2018/7/31.
 */
@Component
public class MendingDataCollectJob {
    private static final Logger logger = LoggerFactory.getLogger(MendingDataCollectJob.class);

    @Autowired
    private MendingDataService mendingDataService;

    @Autowired
    private AdapterService adapterService;

    @Autowired
    private LoadCimConfigService loadCimConfigService;


    @Scheduled(fixedDelayString = MENDING_SCHEDULED_FIXED_DELAY)
    public void run() {
        if (logger.isDebugEnabled()) {
            logger.debug("MendingDataCollectJob start");
        }
        //1.查询第一条补招事件
        MendingEventEntity mendingEventEntity = mendingDataService.getFirstMendingEventEntity();
        while (mendingEventEntity != null) {
            //2.adapter历史数据查询
            //TODO 获取点表信息,与德龙沟通
            //2.1 获取点表数据
            List<MetricEntity> metricEntityList = loadCimConfigService.findMetricList();
            List<String> originalMetricList = new ArrayList<>();
            for (MetricEntity m : metricEntityList) {
                originalMetricList.add(m.getOriginalMetric());
            }
            //2.2 调取adapter历史数据查询接口
            List<OriginalMetricDTO> originalMetricDTOList = adapterService.getHistoryData(mendingEventEntity.getStartTime(),
                    mendingEventEntity.getEndTime(), originalMetricList, mendingEventEntity.getCycle());
            //3.历史数据写入rmq未cim化队列
            if (originalMetricDTOList != null && originalMetricDTOList.size() > 0) {
                mendingDataService.sendHistoryDataToRabbitMQ(originalMetricDTOList);
            }
            //4.删除补招事件
            mendingDataService.deleteMendingEventEntityById(mendingEventEntity.getId());

            mendingEventEntity = mendingDataService.getFirstMendingEventEntity();
        }
    }
}
