package cn.enncloud.iot.collector.service.impl;

import cn.enncloud.iot.collector.service.LoadCimConfigService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

public class InitLoadCimServiceImpl implements CommandLineRunner {
    private static final Logger logger = LogManager.getLogger(InitLoadCimServiceImpl.class);

    @Autowired
    private LoadCimConfigService loadCimConfigService;

    @Override
    public void run(String... strings) throws Exception {
        if (logger.isInfoEnabled()) {
            logger.info("----------从站点表加载开始---------");
        }
        loadCimConfigService.loadMetric();
        if (logger.isInfoEnabled()) {
            logger.info("----------从站点表加载结束---------");
        }
    }
}
