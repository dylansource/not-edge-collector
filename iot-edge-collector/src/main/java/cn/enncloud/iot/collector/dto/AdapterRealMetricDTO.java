package cn.enncloud.iot.collector.dto;

import java.util.List;

public class AdapterRealMetricDTO {

    private List<AdapterObjectDTO> metricValueList = null;

    private Long time;

    public List<AdapterObjectDTO> getMetricValueList() {
        return metricValueList;
    }

    public void setMetricValueList(List<AdapterObjectDTO> metricValueList) {
        this.metricValueList = metricValueList;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
