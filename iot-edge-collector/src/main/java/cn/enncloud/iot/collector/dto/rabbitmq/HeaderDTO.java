package cn.enncloud.iot.collector.dto.rabbitmq;

public class HeaderDTO {
    private String v;
    private Long t;

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public Long getT() {
        return t;
    }

    public void setT(Long t) {
        this.t = t;
    }

    @Override
    public String toString() {
        return "HeaderDTO{" +
                "v='" + v + '\'' +
                ", t=" + t +
                '}';
    }
}
