package cn.enncloud.iot.collector.dto;

public class AdapterObjectDTO {

    private String originalMetric;

    private Float value;

    public String getOriginalMetric() {
        return originalMetric;
    }

    public void setOriginalMetric(String originalMetric) {
        this.originalMetric = originalMetric;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }
}
