package cn.enncloud.iot.collector.dto.rabbitmq;

import java.util.List;

public class RabbitMqDTO {
    private HeaderDTO h;
    List<BodyDTO> b;

    public HeaderDTO getH() {
        return h;
    }

    public void setH(HeaderDTO h) {
        this.h = h;
    }

    public List<BodyDTO> getB() {
        return b;
    }

    public void setB(List<BodyDTO> b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "RabbitMqDTO{" +
                "h=" + h +
                ", b=" + b +
                '}';
    }
}
