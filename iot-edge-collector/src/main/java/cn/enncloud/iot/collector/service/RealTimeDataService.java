package cn.enncloud.iot.collector.service;

import cn.enncloud.iot.collector.dto.AdapterRealMetricDTO;
import cn.enncloud.iot.collector.dto.OriginalMetricDTO;
import cn.enncloud.iot.collector.dto.StationInfoDTO;
import cn.enncloud.iot.collector.dto.rabbitmq.RabbitMqDTO;
import cn.enncloud.iot.domain.entity.MetricEntity;

import java.net.MalformedURLException;
import java.util.List;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
public interface RealTimeDataService {

    /**
     * 查询适配器实时数据
     *
     * @param originalMetricDTO
     * @return
     */
//    List<OriginalMetricDTO> getRealTimeData(List<String> originalMetricDTO);
    AdapterRealMetricDTO getRealTimeData(List<String> originalMetricDTO);

    /**
     * 实时数据写入rabbitmq
     *
     * @param rabbitMqDTO
     * @return
     */
    boolean sendToRabbitmq(RabbitMqDTO rabbitMqDTO);

    /**
     * 更新实时数据时间戳
     *
     * @param statusKey 实时数据状态键值，第一版固定为：real_time_default
     * @param timestamp 最新时间戳
     */
    void updateRealTimeStamp(String statusKey, long timestamp);

    List<MetricEntity> findAll(MetricEntity entity);

    AdapterRealMetricDTO realtimeDataCollector(List<OriginalMetricDTO> originalMetricDTO);

    /**
     * 获取实时数据最新时间戳
     *
     * @param statusKey 实时数据状态键值，第一版固定为：real_time_default
     * @return
     */
    long getRealTimeStamp(String statusKey);

    StationInfoDTO findStationInfo() throws MalformedURLException;
}
