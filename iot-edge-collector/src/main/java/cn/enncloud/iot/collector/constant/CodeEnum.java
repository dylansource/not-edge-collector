package cn.enncloud.iot.collector.constant;

public enum CodeEnum {

    IOT_SUCCESS("10000", "操作成功"),
    IOT_FAIL("10001", "操作失败"),
    IOT_PARAM("10005", "请求参数格式不正确"),
    IOT_EXIST("10006", "数据已存在"),
    IOT_EXCEPTION("99999", "系统异常");


    private String code;
    private String value;

    CodeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
