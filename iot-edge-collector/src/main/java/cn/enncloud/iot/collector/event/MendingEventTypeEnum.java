package cn.enncloud.iot.collector.event;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
public enum MendingEventTypeEnum {

    RabbitmqTimeOut,
    RabbitmqError,
    AdapterTimeOut,
    AdapterError,
    ApplicationError,
    ApplicationStartUp;
}
