package cn.enncloud.iot.collector.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
@Data
@ToString
public class OriginalMetricDTO {

    private String originalMetric;
    private Float value;
    private Long time;

}
