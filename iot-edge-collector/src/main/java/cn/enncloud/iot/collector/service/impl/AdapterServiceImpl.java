package cn.enncloud.iot.collector.service.impl;

import cn.enncloud.iot.collector.dto.OriginalMetricDTO;
import cn.enncloud.iot.collector.dto.query.AdapterHistoryQuery;
import cn.enncloud.iot.collector.dto.response.DataRespBody;
import cn.enncloud.iot.collector.service.AdapterRetrofitService;
import cn.enncloud.iot.collector.service.AdapterService;
import cn.enncloud.iot.commons.util.JsonUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.util.List;

/**
 * @author sunhongqiang
 * @date 2018/08/08
 */
@Service
public class AdapterServiceImpl implements AdapterService {
    private static final Logger logger = LogManager.getLogger(AdapterServiceImpl.class);

    @Autowired
    private Retrofit retrofit;

    /**
     * 查询适配器获取历史数据
     *
     * @param startTime          开始时间
     * @param endTime            结束时间
     * @param originalMetricList 点表list
     * @param interval              采集周期
     * @return 点表历史数据
     */
    @Override
    public List<OriginalMetricDTO> getHistoryData(Long startTime, Long endTime, List<String> originalMetricList, Integer interval) {
        DataRespBody<List<OriginalMetricDTO>> dataRespBody = null;
        List<OriginalMetricDTO> originalMetricDTOList = null;
        AdapterHistoryQuery adapterHistoryQuery = new AdapterHistoryQuery();
        adapterHistoryQuery.setStartTime(startTime);
        adapterHistoryQuery.setEndTime(endTime);
        adapterHistoryQuery.setOriginalMetricList(originalMetricList);
        adapterHistoryQuery.setInterval(interval);
        try {
            AdapterRetrofitService retrofitService = retrofit.create(AdapterRetrofitService.class);
            Call<DataRespBody<List<OriginalMetricDTO>>> call = retrofitService.getHistoryDataFromAdapter(adapterHistoryQuery);
            Response<DataRespBody<List<OriginalMetricDTO>>> response = call.execute();
            dataRespBody = response.body();
            if (dataRespBody.getCode().equals("10000")) {
                originalMetricDTOList = dataRespBody.getData();
            } else {
                logger.error("adapter历史数据查询接口！response:" + JsonUtils.writeValueAsString(dataRespBody));
            }
        } catch (Exception ex) {
            logger.error("adapter历史数据查询接口异常！response:" + JsonUtils.writeValueAsString(dataRespBody), ex);
        }
        return originalMetricDTOList;
    }

    /**
     * 从站原始点查询
     */
    @Override
    public List<String> getMetricFromAdapter() {
        DataRespBody<List<String>> dataRespBody = null;
        List<String> originalMetricList = null;
        try {
            AdapterRetrofitService retrofitService = retrofit.create(AdapterRetrofitService.class);
            Call<DataRespBody<List<String>>> call = retrofitService.getMetricFromAdapter();
            Response<DataRespBody<List<String>>> response = call.execute();
            dataRespBody = response.body();
            if (dataRespBody.getCode().equals("10000")) {
                originalMetricList = dataRespBody.getData();
            } else {
                logger.error("adapter原始点查询接口！response:" + JsonUtils.writeValueAsString(dataRespBody));
            }
        } catch (Exception ex) {
            logger.error("adapter原始点查询接口异常！response:" + JsonUtils.writeValueAsString(dataRespBody), ex);
        }
        return originalMetricList;
    }
}
