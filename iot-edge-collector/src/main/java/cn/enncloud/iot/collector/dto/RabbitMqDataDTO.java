package cn.enncloud.iot.collector.dto;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author sunhongqiang
 * @date 2018/08/06
 * rabbitMq数据格式
 */
@Data
@ToString
public class RabbitMqDataDTO {

    /**
     * header
     */
    private HeaderDTO h;

    /**
     * body
     */
    private List<BodyDTO> b;

}
