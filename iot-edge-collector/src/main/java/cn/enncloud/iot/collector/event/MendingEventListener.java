package cn.enncloud.iot.collector.event;

import cn.enncloud.iot.domain.entity.MendingEventEntity;
import cn.enncloud.iot.domain.repository.MendingEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
@Component
public class MendingEventListener implements ApplicationListener {
    private static final Logger logger = LoggerFactory.getLogger(MendingEventListener.class);

    @Autowired
    private MendingEventRepository mendingEventRepository;

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        if (applicationEvent instanceof MendingEvent) {

            MendingEvent mendingEvent = (MendingEvent) applicationEvent;
            MendingEventEntity mendingEventEntity = new MendingEventEntity();
            mendingEventEntity.setCycle(mendingEvent.getCycle());
            mendingEventEntity.setStartTime(mendingEvent.getStartTime());
            mendingEventEntity.setEndTime(mendingEvent.getEndTime());
            mendingEventEntity.setMendingEventTypeEnum(mendingEvent.getMendingEventTypeEnum());
            // TODO: 2018/7/31 沟通确认开始及结束时间精度
            mendingEventRepository.save(mendingEventEntity);
            if (logger.isDebugEnabled()) {
                logger.debug("保存补招事件，start:{},end:{},id:{}", mendingEventEntity.getStartTime(),
                        mendingEventEntity.getEndTime(), mendingEventEntity.getId());
            }
        }
    }
}
