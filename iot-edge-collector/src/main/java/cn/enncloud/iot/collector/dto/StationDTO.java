package cn.enncloud.iot.collector.dto;

import lombok.Data;

/**
 * @author lifeia
 * @date 2018/8/8.
 */

@Data
public class StationDTO {
    private String stationId;
    private String stationName;
}
