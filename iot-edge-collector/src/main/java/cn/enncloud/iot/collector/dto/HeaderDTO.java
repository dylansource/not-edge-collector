package cn.enncloud.iot.collector.dto;

import lombok.Data;
import lombok.ToString;

/**
 * @author sunhongqiang
 * @date 2018/08/06
 * rabbitMq数据格式head
 */
@Data
@ToString
public class HeaderDTO {

    /**
     * version
     */
    private String v;

    /**
     * time
     */
    private Long t;

}
