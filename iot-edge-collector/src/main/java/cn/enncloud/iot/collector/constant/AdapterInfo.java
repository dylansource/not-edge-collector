package cn.enncloud.iot.collector.constant;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "enn.adapter")
public class AdapterInfo {

    private String url;

    private Integer master;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getMaster() {
        return master;
    }

    public void setMaster(Integer master) {
        this.master = master;
    }

    @Override
    public String toString() {
        return "AdapterInfo{" +
                "url='" + url + '\'' +
                ", master=" + master +
                '}';
    }
}
