package cn.enncloud.iot.collector.event;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
@Data
public class MendingEvent extends ApplicationEvent {

    private long startTime;
    private long endTime;
    private MendingEventTypeEnum mendingEventTypeEnum;
    private int cycle;

    public MendingEvent(String payLoad, long startTime, long endTime, MendingEventTypeEnum mendingEventTypeEnum, int cycle) {
        super(payLoad);
        this.startTime = startTime;
        this.endTime = endTime;
        this.mendingEventTypeEnum = mendingEventTypeEnum;
        this.cycle = cycle;
    }
}
