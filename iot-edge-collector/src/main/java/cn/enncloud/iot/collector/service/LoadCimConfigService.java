package cn.enncloud.iot.collector.service;

import cn.enncloud.iot.domain.entity.MetricEntity;

import java.util.List;

public interface LoadCimConfigService {

    List<MetricEntity> loadMetric();
    List<MetricEntity> findMetricList();
}
