package cn.enncloud.iot.collector.controller;

import cn.enncloud.iot.collector.constant.CodeEnum;
import cn.enncloud.iot.collector.dto.StationDTO;
import cn.enncloud.iot.collector.dto.response.DataRespBody;
import cn.enncloud.iot.collector.dto.response.ResponseBody;
import cn.enncloud.iot.collector.service.MetricService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lifeia
 * @date 2018/7/31.
 */

@RestController
public class MetricController {

    @Autowired
    private MetricService metricService;

    @RequestMapping("api/v1/collector/metric/import")
    public ResponseBody upload(@RequestParam("file") MultipartFile file) throws IOException {

        // 原始文件名
        String sourceName = file.getOriginalFilename();
        Long size = file.getSize();
        String fileType = sourceName.substring(sourceName.lastIndexOf("."));
        String errorInfo = metricService.upload(file.getInputStream(), false);
        DataRespBody responseBody = new DataRespBody<>();
        if (StringUtils.isEmpty(errorInfo)) {
            responseBody.setCode(CodeEnum.IOT_SUCCESS.getCode());
        } else {
            responseBody.setCode(CodeEnum.IOT_FAIL.getCode());
            responseBody.setMsg(CodeEnum.IOT_FAIL.getValue());
            Map<String, String> data = new HashMap<>(1);
            data.put("errorLog ", errorInfo);
            responseBody.setData(data);
        }
        return responseBody;
    }

    @RequestMapping("api/v1/collector/metric/download")
    public ResponseEntity<byte[]> download(HttpServletRequest request) throws Exception {

        String fileName = "点表数据.csv";
        String data = metricService.download();
        byte[] body = data.getBytes("UTF-8");
        //防止中文乱码
        fileName = new String(fileName.getBytes("gbk"), "iso8859-1");
        //设置响应头
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment;filename=" + fileName);
        //设置响应吗
        HttpStatus statusCode = HttpStatus.OK;
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(body, headers, statusCode);
        return response;
    }

    @RequestMapping(value = "api/v1/collector/station", method = RequestMethod.GET)
    public DataRespBody findStationList() {

        List<StationDTO> stationDTOList = metricService.getStationList();
        DataRespBody dataRespBody = new DataRespBody();
        dataRespBody.setCode(CodeEnum.IOT_SUCCESS.getCode());
        dataRespBody.setData(stationDTOList);
        return dataRespBody;
    }

    @RequestMapping(value = "api/v1/collector/deviceType", method = RequestMethod.GET)
    public DataRespBody findDeviceType(@RequestParam(required = false) String stationId) {

        List<String> deviceTypeList = metricService.getDeviceTypeList(stationId);
        DataRespBody dataRespBody = new DataRespBody();
        dataRespBody.setCode(CodeEnum.IOT_SUCCESS.getCode());
        dataRespBody.setData(deviceTypeList);
        return dataRespBody;
    }
}
