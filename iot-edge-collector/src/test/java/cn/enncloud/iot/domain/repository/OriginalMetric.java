package cn.enncloud.iot.domain.repository;

import java.util.List;

public class OriginalMetric {
    List<String> originalMetricList;

    public List<String> getOriginalMetricList() {
        return originalMetricList;
    }

    public void setOriginalMetricList(List<String> originalMetricList) {
        this.originalMetricList = originalMetricList;
    }
}
