package cn.enncloud.iot.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author lifeia
 * @date 2018/7/31.
 */
@SpringBootApplication
public class DomainApplication {
    private static final Logger logger = LoggerFactory.getLogger(DomainApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DomainApplication.class, args);
        logger.info("JVM initialized memory, usedMemory:{}MB, totalMemory:{}MB, maxMemory:{}MB",
                (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024 / 1024,
                Runtime.getRuntime().totalMemory() / 1024 / 1024, Runtime.getRuntime().maxMemory() / 1024 / 1024);
    }
}
