package cn.enncloud.iot.domain;

import cn.enncloud.iot.collector.dto.AdapterRealMetricDTO;
import cn.enncloud.iot.collector.dto.response.DataRespBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class RealTimeDataTest {

    private static final Logger logger = LogManager.getLogger(RealTimeDataTest.class);


    @Test
    public void getRealTimeData() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        String url = "http://10.4.94.46:8080";

        RestTemplate restTemplate = new RestTemplate();

        List<String> list = new ArrayList<>();
        list.add("S1\\B\\AV1_feedback");
        list.add("S1\\B\\T1");
        Map<String, String> map = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        String rs = "";
        try {
            rs = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        map.put("originalMetricList", rs);
        url = url + "?originalMetricList=" + rs;

        HttpEntity<List<String>> entity = new HttpEntity<>(list, headers);
//        List<Object> res = restTemplate.getForObject(url,List.class,map);
        AdapterRealMetricDTO res = restTemplate.getForObject(url, AdapterRealMetricDTO.class);
        logger.info(MessageFormat.format("返回参数【{}】", res.getTime()));

    }

    @Test
    public void getRealTimeDataPost() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        String url = "http://10.4.94.46:8080/";

        RestTemplate restTemplate = new RestTemplate();

        List<String> list = new ArrayList<>();
        list.add("S1\\B\\AV1_feedback");
        list.add("S1\\B\\T1");
        Map<String, List<String>> map = new HashMap<>();
        map.put("originalMetricList", list);

        HttpEntity<List<String>> entity = new HttpEntity<>(list, headers);
        ResponseEntity<String> res = restTemplate.postForEntity(url, entity, String.class);
        logger.info(MessageFormat.format("返回参数【{}】", res.getBody()));

    }

    @Test
    public void getRealTimeDataMy() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        String url = "http://localhost:8080/my/aaa";
//        String url = "http://localhost:8080/my/bbb";

        RestTemplate restTemplate = new RestTemplate();

        List<String> list = new ArrayList<>();
        list.add("S1\\B\\AV1_feedback");
        list.add("S1\\B\\T1");
        Map<String, String> map = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        String rs = "";
        try {
            rs = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        rs = rs.replace("\\\\", "\\");
        map.put("originalMetricList", rs);
        url = url + "?originalMetricList=" + rs;

        HttpEntity<List<String>> entity = new HttpEntity<>(list, headers);
        List<Object> res = restTemplate.getForObject(url, List.class, map);
//        List<Object> res = restTemplate.getForObject(url,List.class);
        logger.info(MessageFormat.format("返回参数【{}】", res.size()));

    }

    @Test
    public void getRealTimeData2() {

        OkHttpClient okHttpClient = new OkHttpClient();


//        String url = "http://10.4.94.46:8080/api/v1/adapter/data/realtime";
        String url = "http://10.39.10.138:8080/api/v1/adapter/data/realtime";

        RestTemplate restTemplate = new RestTemplate();

        List<String> list = new ArrayList<>();
        list.add("S1\\B\\AV1_feedback");
        list.add("S1\\B\\T1");
        list.add("GL2\\JNQ_JYW");
        ObjectMapper mapper = new ObjectMapper();
        String rs = "";
        try {
            rs = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        url = url + "?originalMetricList=" + rs;

        String res = "";
        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            res = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info(MessageFormat.format("adapter返回参数【{0}】", res));

        AdapterRealMetricDTO adapterRealMetricDTO = null;
        DataRespBody<AdapterRealMetricDTO> respBody = null;
        try {
            respBody = mapper.readValue(res, DataRespBody.class);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void getMy() {
        logger.info("时间戳" + System.currentTimeMillis());
    }
}
