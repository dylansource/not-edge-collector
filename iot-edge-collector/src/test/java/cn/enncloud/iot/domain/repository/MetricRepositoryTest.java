package cn.enncloud.iot.domain.repository;

import cn.enncloud.iot.domain.DomainApplication;
import cn.enncloud.iot.domain.entity.MetricEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <p>Title: MetricRepositoryTest.java</p>
 * <p>Description:  MetricRepository单元测试用例</p>
 * <p>Copyright: 新智云数据服务有限公司 2017 沪ICP备16024103号-1</p>
 *
 * @author lixiangk
 * @version 1.0 2018/7/31
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DomainApplication.class)
@Slf4j
public class MetricRepositoryTest {

    @Autowired
    MetricRepository metricRepository;


    @Test
    public void testSave() {
        long prefixNum;
        long recordCount = metricRepository.count();
        if (recordCount == 0) {
            prefixNum = 1;
        } else {
            //查询主键ID的最大值+1
            Pageable pageable = new PageRequest(Long.valueOf(recordCount - 1).intValue(), 1);
            Page<MetricEntity> pageData = metricRepository.findAll(pageable);
            MetricEntity first = pageData.getContent().get(0);
            prefixNum = first.getId() + 1;
        }
        int count = 10;
        for (int i = 1; i <= count; i++) {
            MetricEntity entity = new MetricEntity();
            entity.setOriginalMetric(prefixNum + "-old/aaa/bbb");
            entity.setCimMetric(prefixNum + "-type/deviceId/metric");
            entity.setStationId(prefixNum + "-EA01ES01");
            entity.setName(prefixNum + "-测试");
            entity.setDeviceType("test");
            entity = metricRepository.save(entity);
            entity = metricRepository.findOne(entity.getId());
            Assert.assertNotNull(entity);
            log.debug("第{}条数据保存成功：{}", i, entity.toString());
            prefixNum++;
        }
    }

    @Test
    public void testDeleteAll() {
        metricRepository.deleteAll();
        log.debug("删除全部数据成功！");
    }
}